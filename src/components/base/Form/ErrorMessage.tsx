import classNames from "classnames";
import { FieldMetaProps } from "formik";

export const InputErrorText = ({
                                   meta,
                                   className,
                                   errorMessage
                               }: {
    meta: Partial<FieldMetaProps<any>>;
    errorMessage?: string;
    className: string;
}) => {
    return (
        <p
            className={classNames(
                className,
                "text-red-700 text-sm mt-1",
                (meta.error && meta?.touched)||errorMessage ? "" : "invisible"
            )}
        >
            {meta.error && meta?.touched ? meta.error : "noerror"}
            {errorMessage && errorMessage}
        </p>
    );
};

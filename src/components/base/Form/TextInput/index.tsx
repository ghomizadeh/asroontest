import classNames from "classnames";
import { useField } from "formik";
import { forwardRef, memo } from "react";
import { InputErrorText } from "../ErrorMessage";
import { InputCommonType, TextInputCommonType } from "../input.type";

type InputProps = TextInputCommonType & InputCommonType;

export const TextInput = memo(
    forwardRef(
        (
            {
                rootClasses,
                labelClasses,
                inputClasses,
                errorClasses,
                wrapperClasses,
                EndAdornment,
                StartAdornment,
                label,
                id,
                required,
                meta,
                errorMessage,
                type = "text",
                ...props
            }: InputProps,
            ref
        ) => {
            return (
                <div className={classNames(rootClasses)}>
                    {label && (
                        <label
                            {...(id && { htmlFor: id })}
                            className={classNames(
                                labelClasses,
                                meta?.touched && meta?.error ? "text-red-700" : ""
                            )}
                        >
                            {/*{!required ? (<span className="text-red-700">* </span>) : null}*/}
                            {label}
                        </label>
                    )}

                        <input
                            ref={ref as any}
                            {...(id && { id })}
                            type={type}
                            className={classNames(
                                inputClasses,
                                meta?.touched && meta?.error ? "borderError" : ""
                            )}
                            {...props}
                        />
                        {EndAdornment && <EndAdornment />}

                </div>
            );
        }
    ),
);

export const TextInputFormik = forwardRef((props: InputProps, ref) => {
    const [field, meta] = useField(props.name);
    return <TextInput {...props} {...field} meta={meta} ref={ref as any} />;
});

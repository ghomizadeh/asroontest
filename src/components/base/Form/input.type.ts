import { FieldMetaProps } from "formik";

// we can use => import { DivProps } from "react-html-props"
//  this allows you to use the type DivProps instead of:
export type TextInputCommonType =  React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
    > &
    React.DetailedHTMLProps<
        React.TextareaHTMLAttributes<HTMLInputElement>,
        HTMLInputElement
        >;

export type InputCommonType = {
    name:string;
    rootClasses?: string;
    labelClasses?: string;
    inputClasses?: string;
    wrapperClasses?: string;
    errorClasses?: string;
    StartAdornment?: React.FC<any>;
    EndAdornment?: React.FC<any>;
    label?: string;
    meta?: Partial<FieldMetaProps<any>>;
    errorMessage?:string
};

import classNames from "classnames";
import { BarWave } from "react-cssfx-loading";
import React from "react";

type AppCommonChild = { children: React.ReactNode };


type BaseButtonType = React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement & AppCommonChild
    >;

interface Props extends BaseButtonType {
    loading: boolean;
    submit: any;
    text?: string;
}
export const Button = ({
                                  loading,
                                  text,
                                  submit,
                                  children,
                                  className,
                                  ...props
                              }: Props) => {
    return (
        <button type={submit}
            {...props}
            // onClick={btnRegister}
            className={classNames(
                "flex justify-center items-center w-full bg-red-600 font-bold text-lg text-white rounded-lg h-12 mt-4",
                className
            )}
        >
            {loading ? (
                <BarWave
                    className={""}
                    color="#ffffff"
                    width="40px"
                    height="15px"
                    duration="0.8s"
                />
            ) : (
                <>
                    {text && <span className={""}>{text}</span>}
                    {children && children}
                </>
            )}
        </button>
    );
};

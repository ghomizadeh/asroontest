import {createContext, useEffect, useState} from "react";
import { BiTrash } from 'react-icons/bi';
import { FiEdit } from 'react-icons/fi';
import UserDataService from "./../../../services/Users.service"
import Modal from "../modal/reIndex"
import {Link} from 'react-router-dom';
function DynamicTable(){
    const [users,setUsers] = useState([]);
    const [modalFlag,setModalFlag] = useState(false);
    function handlerChanged(){
        setModalFlag(!modalFlag);
    }
    function handlerClose(){
        setModalFlag(!modalFlag);
    }
    async function getUsers(){
        await UserDataService.getUsers()
            .then((response: any) => {
                setUsers(response.data)
            });
    }
    const tdData =() =>{
        return users.map((data:any,key:number)=>{
            return(
                <tr className={""} key={key}>
                    <td key={key} className={"bg-white h-14 text-sm pr-6 border-t-2 text-asroon-table"}>{data.name}</td>
                    <td key={key} className={"bg-white h-14 text-sm pr-6 border-t-2 text-asroon-table"}>{data.phone}</td>
                    <td key={key} className={"bg-white h-14 text-sm pr-6 border-t-2 text-asroon-table"}>{data.age}</td>
                    <td key={key} className={"bg-white h-14 text-sm pr-6 border-t-2 text-asroon-table"}>{data.email}</td>
                    <td key={key} className={"bg-white h-14 text-sm pr-6 border-t-2 text-asroon-table"}>{new Date().getFullYear()}/{new Date().getMonth()}/{new Date().getDay()}</td>
                    <td className={"bg-white h-14 text-sm pr-6 border-t-2"}>
                        <div className={"flex  gap-8"}>
                            <Link to={"/edit/" + data.id}><FiEdit size={16}/></Link>
                            <Modal onClose={handlerClose}  show={modalFlag} id={data.id}/>
                            <button><BiTrash className={"text-Asroon"} onClick={handlerChanged}  size={16} /></button>
                        </div>
                    </td>
                </tr>
            )
        })
    }
    useEffect(()=>{
        getUsers();
    },[])
    return (
            <table className="table w-3/4 text-right  shadow-md">
                <thead className={"bg-table-header h-14   text-xs"}>
                <tr>
                    <th className="pr-6 text-asroon-table">نام و نام خانوادگی</th>
                    <th className="pr-6 text-asroon-table">شماره موبایل</th>
                    <th className="pr-6 text-asroon-table">سن</th>
                    <th className="pr-6 text-asroon-table">ایمیل</th>
                    <th className="pr-6 text-asroon-table">تاریخ ایجاد</th>
                    <th className="pr-6 text-asroon-table"> </th>
                </tr>
                </thead>
                <tbody className={""}>
                {tdData()}
                </tbody>
            </table>
    )
}
export default DynamicTable;


import React from "react";

class Modal extends React.Component {
    onClose = e => {
        this.props.onClose && this.props.onClose(e);
    };
    render(){
        //if show is set to false, don't render
        if (!this.props.show) {
            return null;
        }
        return (
            <>
                <div className={"fixed z-10 inset-0 overflow-y-auto"} aria-labelledby="modal-title" role="dialog" aria-modal="false">
                    <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                        <div className="fixed inset-0 bg-gray-500 bg-opacity-25 transition-opacity" aria-hidden="true"></div>
                        <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
                        <div className="inline-block align-bottom bg-white rounded text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                            <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                <div className="sm:flex sm:items-start">
                                    <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left w-full">
                                        <div className={"flex justify-between"}>
                                            <div>
                                            <span className={"font-bold text-lg"}>
                                                حذف ردیف
                                            </span>
                                            </div>
                                            <button onClick={this.onClose}>
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M18 5.99997C17.8124 5.8125 17.5581 5.70718 17.293 5.70718C17.0278 5.70718 16.7735 5.8125 16.586 5.99997L12 10.586L7.41397 5.99997C7.22644 5.8125 6.97213 5.70718 6.70697 5.70718C6.44181 5.70718 6.1875 5.8125 5.99997 5.99997C5.8125 6.1875 5.70718 6.44181 5.70718 6.70697C5.70718 6.97213 5.8125 7.22644 5.99997 7.41397L10.586 12L5.99997 16.586C5.8125 16.7735 5.70718 17.0278 5.70718 17.293C5.70718 17.5581 5.8125 17.8124 5.99997 18C6.1875 18.1874 6.44181 18.2928 6.70697 18.2928C6.97213 18.2928 7.22644 18.1874 7.41397 18L12 13.414L16.586 18C16.7735 18.1874 17.0278 18.2928 17.293 18.2928C17.5581 18.2928 17.8124 18.1874 18 18C18.1874 17.8124 18.2928 17.5581 18.2928 17.293C18.2928 17.0278 18.1874 16.7735 18 16.586L13.414 12L18 7.41397C18.1874 7.22644 18.2928 6.97213 18.2928 6.70697C18.2928 6.44181 18.1874 6.1875 18 5.99997Z" fill="#000"/>
                                                </svg>
                                            </button>
                                        </div>
                                        <div className={"w-full text-right mt-4"}>
                                            <div>
                                            <span className={"font-bold text-lg"}>
                                               آیا از حذف ردیف مطمن هستید؟
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="flex justify-between bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse mt-6">
                                <div className="">
                                    <button onClick={this.onClose} type="button" className="mt-3 w-full inline-flex justify-center rounded-lg border border-gray-300 shadow-sm px-6 py-2 bg-white text-base font-medium text-gray-700 hover:bg-red-600 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">

                                        <span>حذف</span>
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default Modal;

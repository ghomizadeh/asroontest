import { createContext, useContext } from 'react';
import {IUser} from "../types/users.type";
export const contextDefaultValues: IUser = {
    name: "",
    phone: "",
    age: "",
    email: "",
};
export const UsersContext = createContext<IUser>(contextDefaultValues);


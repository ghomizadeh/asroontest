import React, {useEffect, useState} from "react"
import { useNavigate} from 'react-router-dom';
import { Form, Formik } from "formik";
import * as Yup from "yup";
import {Logo} from "../components/base/logo"
import {Button} from "../components/base/button"
import { TextInputFormik } from "../components/base/Form/TextInput";
import { useParams } from 'react-router-dom';
import UserDataService from "./../services/Users.service"

const INITIAL_VALUES = {
    name: "mohammad",
    email: "",
    age: "",
    phone: "",
};

function Edit(prp:any) {
    let userParm = useParams();
    const navigate  = useNavigate();
    const [user,setUser] = useState<any>([])
  
    const validationSchema = Yup.object().shape({
        name: Yup.string().required("الزامی می باشد"),
        email: Yup.string().required("الزامی می باشد"),
        phone: Yup.number().required("الزامی می باشد"),
        age: Yup.number().required("الزامی می باشد"),

    });

    async function getUser(id:any){
        await UserDataService.getUser(id)
            .then((response: any) => {
                setUser(response.data)
            });
    }


    
    function onSubmit(values: typeof INITIAL_VALUES) {
        navigate("/show")
    }
    useEffect(()=>{
        getUser(userParm.id);

    },[])
    return (
        <>
            <div className="container">
                {/* marginTop in figma size : mt-[116px]*/}
                <div className="flex flex-col justify-center items-center mt-16">
                    <Logo width={125.4} height={132} url={"/"} />
                    <Formik
                        enableReinitialize={true}
                        validationSchema={validationSchema}
                        onSubmit={onSubmit}
                        initialValues={user}
                    >

                        <Form>
                            <div className={"bg-white w-97  mt-8 p-4 rounded shadow-md"}>
                                <span className={"font-bold text-lg"}>فرم زیر را پر کنید.</span>

                                <TextInputFormik
                                    name="name"
                                    required
                                    placeholder="نام و نام خانوادگی شما"
                                    label="نام و نام خانوادگی"
                                    labelClasses={"text-xs"}
                                    inputClasses={"mt-1 pr-2 text-lg w-full h-12 border rounded"}
                                    rootClasses={"mt-2"}

                                />
                                <TextInputFormik
                                    name="phone"
                                    required
                                    placeholder="شماره موبایل"
                                    label="شماره موبایل"
                                    labelClasses={"text-xs"}
                                    inputClasses={"mt-1 pr-2 text-lg w-full h-12 border rounded"}
                                    rootClasses={"mt-2"}

                                />
                                <TextInputFormik
                                    name="age"
                                    required
                                    placeholder="سن"
                                    label="سن"
                                    labelClasses={"text-xs"}
                                    inputClasses={"mt-1 pr-2 text-lg w-full h-12 border rounded"}
                                    rootClasses={"mt-2"}

                                />
                                <TextInputFormik
                                    name="email"
                                    required
                                    placeholder="ایمیل"
                                    label="ایمیل"
                                    labelClasses={"text-xs"}
                                    inputClasses={"mt-1 pr-2 text-lg w-full h-12 border rounded"}
                                    rootClasses={"mt-2"}

                                />
                                <Button loading={false} submit={"submit"} text={"ثبت اطلاعات"}></Button>
                            </div>
                        </Form>
                    </Formik>
                </div>
            </div>
        </>
    )
}
export default  Edit;

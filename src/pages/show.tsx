import React from "react"
import * as Yup from "yup";
import {Logo} from "../components/base/logo"
import {CreateNewAccountButton} from "../components/base/button/new"
import {FetchApiButton} from "../components/base/button/fetch"
import DynamicTable from "../components/base/dynamicTable"





function Show() {
    return (
        <>
            <div className="container">
                <div className="flex flex-col justify-center items-center mt-16">
                    <Logo width={125.4} height={132} url={"/"} />
                    <div className={"flex justify-between items-center w-3/4"}>
                        <div><span className={"text-lg font-bold"}>داده ها</span></div>
                        <div className={"flex gap-2"}>
                            <FetchApiButton />
                            <CreateNewAccountButton />
                        </div>
                    </div>
                    <div className={"flex justify-center w-full mt-4"}>
                        <DynamicTable />
                    </div>
                </div>
            </div>
        </>
    )
}
export default  Show;

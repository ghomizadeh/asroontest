import * as React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Create from "../pages/create"
import Show from "../pages/show"
import Edit from "../pages/edit"
const Router = () => {
    return (
        <BrowserRouter>
            <Routes >
                <Route  path="/" element={<Create />} />
                <Route  path="/show" element={<Show />} />
                <Route  path="/edit/:id" element={<Edit />} />
            </Routes >
        </BrowserRouter>
    );
};

export default Router;

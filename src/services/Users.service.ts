import api from "../utils/config";
class UserDataService {
    getUsers() {
        return api.get<Array<any>>("/users");
    }
    getUser(id:number) {
        return api.get<Array<any>>("/users/" + id);
    }
}
export default new UserDataService();

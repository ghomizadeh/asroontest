export type IUser = {
    email: string,
    phone: string,
    age: string,
    name: string,
}

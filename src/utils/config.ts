// import * as dotenv from "dotenv"; // Just install package => dotenv.config();
import axios from "axios";
export default axios.create({
    baseURL:"https://626789d1786383364220da6c.mockapi.io/api/v1/asroon",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    },
    withCredentials: false
});

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    container:{
      center:true,
    },
    extend: {
      colors:{
        "asroon-table":"#666464",
        "Asroon":"#DE2D26",
      },
      backgroundColor:{
        "table-header":"#F5F5F5"
      },
      margin:{
        "116px":"116px"
      },
      width:{
        '97':'27rem',
      },
    },
  },
  plugins: [],
}
